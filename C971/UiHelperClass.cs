﻿using C971.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C971
{
    public static class UiHelperClass
    {
        public static Button CreateButton(Term t)
        {
            var res = new Button
            {
                Text = t.Name,
                Padding = 7,
                BackgroundColor = Colors.MediumPurple,
                TextColor = Colors.Black,
                CornerRadius = 7,
            };
            return res;
        }

        public static Button CreateAddTermButton()
        {
            return new Button()
            {
                Text = "Add Term",
                Padding = 7,
                BackgroundColor = Colors.MediumPurple,
                TextColor = Colors.Black,
                CornerRadius = 7,
            };
        }

        public static Button CreateCourseButton(Course c)
        {
            return new Button
            {
                Text = c.Name
            };
        }

        public static SwipeItem CreateDeleteSwipeItem(Course c)
        {
            var res = new SwipeItem
            {
                Text = "Delete",
                BindingContext = c,
                BackgroundColor = Colors.LightCoral

            };

            return res;
        }
        public static Grid CreateWhiteGrid()
        {
            return new Grid
            {
                BackgroundColor = Colors.White
            };
        }
        public static SwipeItem CreateShareSwipeItem(Note note)
        {
            var res = new SwipeItem
            {
                Text = "Share",
                BindingContext = note,
                BackgroundColor = Colors.LightBlue
            };
            return res;
        }

        public static SwipeItem CreateDeleteSwipeItem(Note note)
        {
            var res = new SwipeItem
            {
                Text = "Delete",
                BindingContext = note,
                BackgroundColor = Colors.LightCoral
            };
            return res; 
        }

        public static SwipeView CreateSwipeView(Note note, List<SwipeItem> itemList)
        {
            var grid = new Grid
            {
                BackgroundColor = Colors.LightBlue
            };

            grid.Add(new Label
            {
                Text = note?.Content
            });

            var swp = new SwipeView
            {
                RightItems = new SwipeItems(itemList),
                Content = grid
            };

            return swp;
        }

        public static SwipeView CreateSwipeView2(List<SwipeItem> items, View content)
        {
            var res = new SwipeView
            {
                RightItems = new SwipeItems(items),
                Content = content
            };
            return res;
        }

        public static Button CreateAddCourseButton()
        {
            var res = new Button()
            {
                Text = "Add Course",
            };

            return res;
        }

        public static Button CreateDeleteCourseButton()
        {
            var res = new Button()
            {
                Text = "Delete Term",
                BackgroundColor = Colors.Red
            };

            return (Button)res;
        }

    }
}

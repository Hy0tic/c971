﻿using C971.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C971
{
    public static class CourseDetailPageHelper
    {
        public static void UpdateCourse(Course c)
        {
            DF.EditCourse(c);
            MainPage.SyncDB();
        }

        public static void UpdateInstructor(Instructor i)
        {
            DF.EditInstructor(i);
            MainPage.SyncDB();
        }

        public static void UpdateNote(Note n)
        {
            DF.EditNote(n);
            MainPage.SyncDB();
        }

        public static void UpdateAssessment(Assessment a)
        {
            DF.EditAssessment(a);
            MainPage.SyncDB();
        }


    }
}

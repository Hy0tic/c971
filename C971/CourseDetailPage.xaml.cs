using SQLite;
using C971.Models;
using C971;
using static System.Net.Mime.MediaTypeNames;

namespace C971;

public partial class CourseDetailPage : ContentPage
{
    public Course CurrentCourse;
    public Assessment PA;
    public Assessment OA;
    public Instructor CurrentInstructor;
    public CourseDetailPage(int courseId)
    {
       InitializeComponent();
       var course = MainPage.CourseDictionary[courseId];
        CurrentInstructor = MainPage.InstructorDictionary[course.InstructorId];
        CurrentCourse = course;

       PA = MainPage.AssessmentDictionary[course.Pa];
       OA = MainPage.AssessmentDictionary[course.Oa];

       FillCourseInfo(course);
       FillInstructorInfo();
       FillOaAndPaInfo();
       FillNote();

    }

    private void SyncDatabase()
    {
       MainPage.SyncDB();
    }

    private void FillOaAndPaInfo()
    {
       oaDueDate.Date = OA.DueDate;
       oaEndNotif.SelectedItem = OA.EndNotif;
       oaStartNotif.SelectedItem = OA.StartNotif;
       oaName.Text = OA.Name;
       oaEndNotif.ItemsSource = MainPage.NotificationList;
       oaStartNotif.ItemsSource = MainPage.NotificationList;
       oaStart.Date = OA.Start;
       oaEnd.Date = OA.End;

       paEnd.Date = PA.End;
       paStartNotif.SelectedItem = PA.StartNotif;
       paEndNotif.ItemsSource = MainPage.NotificationList;
       paStartNotif.ItemsSource = MainPage.NotificationList;
       paStart.Date = PA.Start;
       paDueDate.Date = PA.DueDate;
       paEndNotif.SelectedItem = PA.EndNotif;
       paName.Text = PA.Name;
    }

    private void FillCourseInfo(Course course)
    {
       CourseTitleEntry.Text = course.Name;
       CourseStartDate.Date = course.Start;
       CourseEndDate.Date = course.End;
       Status.ItemsSource = EnumExtensions.GetDisplayNames<Status>(); ;
       Status.SelectedItem = course.Status;
       CourseStartNotif.ItemsSource = MainPage.NotificationList;
       CourseStartNotif.SelectedItem = course.StartNotification;
       CourseEndNotif.SelectedItem = course.EndNotification;
       CourseEndNotif.ItemsSource = MainPage.NotificationList;
       DetailsField.Text = course.Details;
    }

    private void FillInstructorInfo()
    {
       InstructorName.Text = CurrentInstructor.Name;
       InstructorPhone.Text = CurrentInstructor.PhoneNumber;
       InstructorEmail.Text = CurrentInstructor.EmailAddress;
    }

    public void FillNote()
    {
       NoteVerticalStack.Children.Clear();

       var note = MainPage.NoteDictionary.Values
                   .Where(note => note.CourseId == CurrentCourse.Id)
                   .FirstOrDefault();

       var itemToShare = UiHelperClass.CreateShareSwipeItem(note);

       var deleteItem = UiHelperClass.CreateDeleteSwipeItem(note);

       itemToShare.Invoked += OnShare;

       deleteItem.Invoked += OnDelete;

       var items = new List<SwipeItem>() { itemToShare, deleteItem };

       var swp = UiHelperClass.CreateSwipeView(note, items);

       NoteVerticalStack.Add(swp);
    }

    public void OnDelete(object senderObj, EventArgs @event)
    {
       var i = (SwipeItem)senderObj;
       var n = (Note)i.BindingContext;
       DF.DeleteNote(n);
       SyncDatabase();
       FillNote();
    }

    public async void OnShare(object senderObj, EventArgs @event)
    {
       var i = (SwipeItem)senderObj;
       var n = (Note)i.BindingContext;
       await NotifHelperClass.ShareNote(n.Content);
       SyncDatabase();
    }

    public void OnCourseStartDateSelect(object senderObj, DateChangedEventArgs @event)
    {
       var isValid = MainPage.CheckDate(CourseStartDate.Date, CourseEndDate.Date);
       if (isValid)
       {
           CurrentCourse.Start = @event.NewDate;
           CourseDetailPageHelper.UpdateCourse(CurrentCourse);
       }

    }

    public void OnCourseEndDateSelect(object senderObj, DateChangedEventArgs @event)
    {
       var isValid = MainPage.CheckDate(CourseStartDate.Date, CourseEndDate.Date);
       if (isValid)
       {
           CurrentCourse.End = @event.NewDate;
           CourseDetailPageHelper.UpdateCourse(CurrentCourse);
       }
    }

    public void OnCourseStatusChange(object senderObj, EventArgs @event)
    {
       CurrentCourse.Status = (string)Status.SelectedItem;
       CourseDetailPageHelper.UpdateCourse(CurrentCourse);
    }

    public void OnCourseTitleChange(object senderObj, TextChangedEventArgs @event)
    {
       CurrentCourse.Name = CourseTitleEntry.Text;
       CourseDetailPageHelper.UpdateCourse(CurrentCourse);
    }

    public void OnInstructorEmailChange(object senderObj, TextChangedEventArgs @event)
    {
        if (string.IsNullOrWhiteSpace(@event.NewTextValue))
        {
            Warning.Text = "Email cannot be empty.";
        }
        else
        {
           Warning.Text = "";
           CurrentInstructor.EmailAddress = @event.NewTextValue;
           CourseDetailPageHelper.UpdateInstructor(CurrentInstructor);
        }
    }

    public void OnInstructorPhoneChange(object senderObj, TextChangedEventArgs @event)
    {
        if (string.IsNullOrWhiteSpace(@event.NewTextValue))
        {
            Warning.Text = "Phone number cannot be empty.";
        }
        else
        {
            Warning.Text = "";
           CurrentInstructor.PhoneNumber = @event.NewTextValue;
           CourseDetailPageHelper.UpdateInstructor(CurrentInstructor);
        }
    }

    public void OnInstructorNameChange(object senderObj, TextChangedEventArgs @event)
    {
        if (string.IsNullOrWhiteSpace(@event.NewTextValue))
        {
            Warning.Text = "Instructor name cannot be empty.";
        }
        else
        {
            Warning.Text = "";
            CurrentInstructor.Name = @event.NewTextValue;
            CourseDetailPageHelper.UpdateInstructor(CurrentInstructor);
        }
    }

    public void AddNote(object senderObj, EventArgs @event)
    {
       if (NoteEntry.Text != null)
       {
           var note = new Note(CurrentCourse.Id, NoteEntry.Text);
           CourseDetailPageHelper.UpdateNote(note);
       }
       NoteEntry.Text = "";
       FillNote();
    }

    public void CourseStartNotifIndexChange(object senderObj, EventArgs @event)
    {
       CurrentCourse.StartNotification = (int)CourseStartNotif.SelectedItem;
       CourseDetailPageHelper.UpdateCourse(CurrentCourse);
    }

    public void CourseEndNotifIndexChange(object senderObj, EventArgs @event)
    {
       CurrentCourse.EndNotification = (int)CourseEndNotif.SelectedItem;
       CourseDetailPageHelper.UpdateCourse(CurrentCourse);

    }

    public void CourseDetailChange(object senderObj, TextChangedEventArgs @event)
    {
       CurrentCourse.Details = DetailsField.Text;
       CourseDetailPageHelper.UpdateCourse(CurrentCourse);
    }

    public void PaStartNotifIndexChange(object senderObj, EventArgs @event)
    {
       PA.StartNotif = (int)paStartNotif.SelectedItem;
       CourseDetailPageHelper.UpdateAssessment(PA);

    }

    public void PaEndNotifIndexChange(object senderObj, EventArgs @event)
    {
       PA.EndNotif = (int)paEndNotif.SelectedItem;
       CourseDetailPageHelper.UpdateAssessment(PA);
    }

    public void OaStartNotifIndexChange(object senderObj, EventArgs @event)
    {
       OA.StartNotif = (int)oaStartNotif.SelectedItem;
       CourseDetailPageHelper.UpdateAssessment(OA);

    }

        public void PaNameChange(object senderObj, TextChangedEventArgs @event)
    {
       if (@event.NewTextValue != null)
       {
           PA.Name = @event.NewTextValue;
           CourseDetailPageHelper.UpdateAssessment(PA);
       }
    }

    public void OaNameChange(object senderObj, TextChangedEventArgs @event)
    {
       if (@event.NewTextValue != null)
       {
           OA.Name = @event.NewTextValue;
           CourseDetailPageHelper.UpdateAssessment(OA);
       }

    }

    public void PaDueDateSelect(object senderObj, DateChangedEventArgs @event)
    {
       PA.DueDate = @event.NewDate;
       CourseDetailPageHelper.UpdateAssessment(PA);
    }

    public void OaDueDateSelect(object senderObj, DateChangedEventArgs @event)
    {
       OA.DueDate = @event.NewDate;
       CourseDetailPageHelper.UpdateAssessment(OA);
    }

    public void OaEndDateSelect(object senderObj, DateChangedEventArgs @event)
    {
       OA.End = @event.NewDate;
       CourseDetailPageHelper.UpdateAssessment(OA);
    }

    public void OaStartDateSelect(object senderObj, DateChangedEventArgs @event)
    {
       OA.Start = @event.NewDate;
       CourseDetailPageHelper.UpdateAssessment(OA);
    }


    public void PaStartDateSelect(object senderObj, DateChangedEventArgs @event)
    {
       PA.Start = @event.NewDate;
       CourseDetailPageHelper.UpdateAssessment(OA);
    }
    public void OaEndNotifIndexChange(object senderObj, EventArgs @event)
    {
       OA.EndNotif = Convert.ToInt32(oaEndNotif.SelectedItem);
       CourseDetailPageHelper.UpdateAssessment(OA);
    }

    public void PaEndDateSelect(object senderObj, DateChangedEventArgs @event)
    {
       PA.End = @event.NewDate;
       CourseDetailPageHelper.UpdateAssessment(PA);
    }
}
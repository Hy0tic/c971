﻿using Plugin.LocalNotification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C971.Models;

namespace C971
{
    public static class NotifHelperClass
    {

        public static NotificationRequest CreateStartingNotifRequest(Course c)
        {
            var now = DateTime.Now;
            var res = new NotificationRequest()
            {
                NotificationId = c.Id + 1000,
                Title = "Course is starting",
                Description = c.Name + " Starting soon",
                Schedule = new NotificationRequestSchedule()
                {
                    NotifyTime = c.Start.AddDays(-c.StartNotification).AddHours(now.Hour).AddMinutes(now.Minute + 1),
                    RepeatType = NotificationRepeat.Daily
                }
            };

            return res;
        }

        public static NotificationRequest CreateEndingNotifRequest(Course c)
        {
            var now = DateTime.Now;
            var res = new NotificationRequest()
            {
                NotificationId = c.Id + 2000,
                Title = "Course is ending",
                Description = c.Name + " Ending soon",
                Schedule = new NotificationRequestSchedule()
                {
                    NotifyTime = c.End.AddDays(-c.EndNotification).AddHours(now.Hour).AddMinutes(now.Minute + 1),
                    RepeatType = NotificationRepeat.Daily
                }
            };
            return res;
        }

        public static NotificationRequest CreateStartingNotifRequest(Assessment a)
        {
            var now = DateTime.Now;
            var res = new NotificationRequest()
            {
                NotificationId = a.Id + 3000,
                Title = "Assessment is starting",
                Description = a.Name + " Starting soon",
                Schedule = new NotificationRequestSchedule()
                {
                    NotifyTime = a.Start.AddDays(-a.StartNotif).AddHours(now.Hour).AddMinutes(now.Minute + 1),
                    RepeatType = NotificationRepeat.Daily
                }
            };

            return res;
        }

        public static NotificationRequest CreateEndingNotifRequest(Assessment a)
        {
            var now = DateTime.Now;
            var res = new NotificationRequest()
            {
                NotificationId = a.Id + 4000,
                Title = "Assessment is ending",
                Description = a.Name + " Ending soon",
                Schedule = new NotificationRequestSchedule()
                {
                    NotifyTime = a.End.AddDays(-a.EndNotif).AddHours(now.Hour).AddMinutes(now.Minute + 1),
                    RepeatType = NotificationRepeat.Daily
                }
            };
            return res;
        }

        public static async Task ShareNote(string note)
        {
            await Share.Default.RequestAsync(
                new ShareTextRequest
                {
                    Text = note
                });
        }

    }
}

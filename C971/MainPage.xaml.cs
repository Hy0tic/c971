﻿using Plugin.LocalNotification;
using System.Collections;
using System.Windows.Input;
using Microsoft.Maui.Graphics;
using C971.Models;
using SQLite;


namespace C971
{
    public partial class MainPage : ContentPage
    {
        public static List<Term> TermList = [];
        public static Dictionary<int, Note> NoteDictionary = [];
        public static Dictionary<int, Course> CourseDictionary = [];
        public static Dictionary<Term, List<Course>> TermCourseDictionary = [];
        public static Dictionary<int, Instructor> InstructorDictionary = [];
        public static Dictionary<int, Assessment> AssessmentDictionary = [];
        public static Term SelectedSemester;
        public static List<int> NotificationList = [1,2,3,4,5,6,7,8];
        public static List<NotificationRequest> NotificationRequestList = [];
        public MainPage()
        {
            InitializeComponent();
            DF.AddExampleData();
            LoadSavedData();
            LoadUI(1);
        }

        public static void LoadSavedData()
        {
            TermList = DF.QueryTerm();
            foreach (var r in TermList)
            {
                var CourseList = DF.QueryCourse(r.Id);
                foreach (var c in CourseList)
                {
                    CourseDictionary.Add(c.Id, c);
                }
                TermCourseDictionary.Add(r, CourseList);
            }

            var assessmentList = DF.QueryAssessment();
            foreach (var a in assessmentList)
            {
                AssessmentDictionary.Add(a.Id, a);
            }


            var instructorList = DF.QueryInstructor();
            foreach (var i in instructorList)
            {
                InstructorDictionary.Add(i.Id, i);
            }

            var noteList = DF.QueryNote();
            foreach (var n in noteList)
            {
                NoteDictionary.Add(n.Id, n);
            }
        }



        public static void SyncDB()
        {
            TermCourseDictionary = [];
            CourseDictionary = [];
            AssessmentDictionary = [];
            InstructorDictionary = [];
            NoteDictionary = [];

            TermList = DF.QueryTerm();
            foreach (var t in TermList)
            {
                var courseInTermList = DF.QueryCourse(t.Id);
                var courseList = new List<Course>();
                foreach (var course in courseInTermList)
                {
                    courseList.Add(course);
                    CourseDictionary.Add(course.Id, course);
                }
                TermCourseDictionary.Add(t, courseList);
            }

            var assessmentList = DF.QueryAssessment();
            foreach (var a in assessmentList)
            {
                AssessmentDictionary.Add(a.Id, a);
            }

            var instructorList = DF.QueryInstructor();
            foreach (var i in instructorList)
            {
                InstructorDictionary.Add(i.Id, i);
            }

            var noteList = DF.QueryNote();
            foreach (var n in noteList)
            {
                NoteDictionary.Add(n.Id, n);
            }

        }

        public void LoadUI(int term)
        {
            TermHorizontalStack.Children.Clear();
            CourseVerticalStack.Children.Clear();
            SelectedSemester = TermList.FirstOrDefault(t => t.Id == term);

            foreach (var t in TermList)
            {
                var button = UiHelperClass.CreateButton(t);
                button.Clicked += void (senderObj, args) => LoadUI(t.Id);
                TermHorizontalStack.Children.Add(button);
            }

            var buttonTermAdd = UiHelperClass.CreateAddTermButton();
            buttonTermAdd.Clicked += void (senderObj, args) => OnNewTerm();
            TermHorizontalStack.Children.Add(buttonTermAdd);

            foreach (var c in TermCourseDictionary[SelectedSemester])
            {
                var grid = UiHelperClass.CreateWhiteGrid();
                var button = UiHelperClass.CreateCourseButton(c);
                button.Clicked += async (senderObj, args) => await Navigation.PushAsync(new CourseDetailPage(c.Id));
                grid.Add(button);

                var deleteItem = UiHelperClass.CreateDeleteSwipeItem(c);
                deleteItem.Invoked += OnDeleteInvoke;

                var items = new List<SwipeItem>() { deleteItem };

                var swp = UiHelperClass.CreateSwipeView2(items, grid);


                CourseVerticalStack.Children.Add(swp);
            }

            if (TermCourseDictionary[SelectedSemester].Count < 6)
            {
                var buttonCourseAdd = UiHelperClass.CreateAddCourseButton();
                buttonCourseAdd.Clicked += void (senderObj, args) => OnNewCourse();
                CourseVerticalStack.Children.Add(buttonCourseAdd);
            }

            if (TermCourseDictionary[SelectedSemester].Count == 0)
            {
                var buttonTermRemove = UiHelperClass.CreateDeleteCourseButton();
                buttonTermRemove.Clicked += void (senderObj, args) => OnTermDelete();
                CourseVerticalStack.Children.Add(buttonTermRemove);
            }

            TermStartDate.Date = SelectedSemester.Start;
            TermEndDate.Date = SelectedSemester.End;
            TermTitle.Text = SelectedSemester.Name;

        }

        public void OnDeleteInvoke(object senderObj, EventArgs @event)
        {
            var item = (SwipeItem)senderObj;
            var course = (Course)item.BindingContext;
            DF.DeleteCourse(course);
            SyncDB();
            LoadUI(SelectedSemester.Id);
        }

        public void TermTitleChange(object senderObj, TextChangedEventArgs @event)
        {
            if (@event.NewTextValue != null)
            {
                SelectedSemester.Name = @event.NewTextValue;
                DF.EditTerm(SelectedSemester);
                LoadUI(SelectedSemester.Id);
            }
        }

        public void SelectTermEnd(object senderObj, DateChangedEventArgs @event)
        {
            var valid = CheckDate(TermStartDate.Date, TermEndDate.Date);
            if (valid)
            {
                TermEndDate.Date = @event.NewDate;
                SelectedSemester.End = @event.NewDate;
                DF.EditTerm(SelectedSemester);
            }
        }
        public void SelectTermStart(object senderObj, DateChangedEventArgs @event)
        {
            var valid = CheckDate(TermStartDate.Date, TermEndDate.Date);
            if (valid)
            {
                TermStartDate.Date = @event.NewDate;
                SelectedSemester.Start = @event.NewDate;
                DF.EditTerm(SelectedSemester);
            }

        }
        protected override void OnAppearing()
        {
            LoadUI(SelectedSemester.Id);
        }
        public void OnNewTerm()
        {
            DF.AddNewTerm();
            LoadUI(SelectedSemester.Id);
        }
        public void OnTermDelete()
        {
            DF.DeleteTerm(SelectedSemester);
            SyncDB();
            LoadUI(1);
        }
        public void OnNewCourse()
        {
            DF.AddNewCourse(SelectedSemester.Id);
            LoadUI(SelectedSemester.Id);
            SyncDB();
        }

        public static bool CheckDate(DateTime _startDate, DateTime _endDate)
        {
            if (_endDate < _startDate)
            {
                return false;
            }

            return true;
        }


    }
}
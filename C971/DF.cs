﻿using C971.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C971
{
    public static class DF
    {
        public static readonly string DbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "YourDatabase.db");
        public static void CreateTable()
        {
            var connection = new SQLiteConnection(DbPath);
            connection.CreateTable<Term>();
            connection.CreateTable<Course>();
            connection.CreateTable<Assessment>();
            connection.CreateTable<Instructor>();
            connection.CreateTable<Note>();
        }

        #region note
        public static void DeleteNote(Note note)
        {
            var connection = new SQLiteConnection(DbPath);
            if (note != null)
            {
                connection.Delete(note);
            }
        }
        public static void EditNote(Note note)
        {
            var connection = new SQLiteConnection(DbPath);
            connection.Update(note);
        }
        public static void AddNote(Note note)
        {
            var connection = new SQLiteConnection(DbPath);
            connection.Insert(note);
        }

        public static List<Note> QueryNote()
        {
            var connection = new SQLiteConnection(DbPath);
            return connection.Query<Note>("SELECT * FROM Note");
        }

        #endregion

        #region course
        public static void AddCourse(Course course) 
        {
            var connection = new SQLiteConnection(DbPath);
            connection.Insert(course);
        }
        public static void AddNewCourse(int termId)
        {
            var connection = new SQLiteConnection(DbPath);
            var PA = new Assessment(1, "PerformanceAssessmentExample", DateTime.Now, DateTime.Now.AddMonths(3), "Enter details here:", 1);
            var OA = new Assessment(0, "ObjectiveAssessmentExample", DateTime.Now, DateTime.Now.AddMonths(3), "Enter details here:", 1);
            var course1 = new Course(termId, 1, "NewCourseExample", DateTime.Now, DateTime.Now.AddMonths(4), "Plan to Take", "Enter Details Here:", 1, 2);
            AddCourse(course1);
            var cList = connection.Query<Course>($"SELECT Id FROM Course WHERE Name='NewCourseExample'");
            PA.Id = cList[0].Id;
            OA.Id = cList[0].Id;
            AddAssessment(PA);
            AddAssessment(OA);
            var resp2 = connection.Query<Assessment>($"SELECT Id FROM Assessment WHERE Id='{cList[0].Id.ToString()}'");
            foreach (Assessment a in resp2)
            {
                if (a.Type == 1)
                {
                    course1.Pa = a.Id;
                }
                else
                {
                    course1.Oa = a.Id;
                }
            }
            connection.Update(course1);
            MainPage.SyncDB();
        }

        public static void EditCourse(Course course) 
        {
            var connection = new SQLiteConnection(DbPath);
            connection.Update(course);
        }
        public static void DeleteCourse(Course course) 
        {
            var connection = new SQLiteConnection(DbPath);
            connection.Delete(course);
        }

        public static List<Course> QueryCourse(int id)
        {
            var connection = new SQLiteConnection(DbPath);
            return connection.Query<Course>($"SELECT * FROM Course WHERE termId={id}");
        }
        #endregion

        #region term
        public static void AddNewTerm()
        {
            var connection = new SQLiteConnection(DbPath);
            var resp = connection.Query<Term>($"SELECT * FROM Term ORDER BY Id DESC LIMIT 1");
            var nt = resp.First();
            var termName = "Term " + (nt.Id + 1).ToString();
            var rt = new Term(termName, DateTime.Now, DateTime.Now.AddDays(60));
            connection.Insert(rt);
            MainPage.SyncDB();
        }
        public static void AddTerm(Term term) 
        {
            var connection = new SQLiteConnection(DbPath);
            connection.Insert(term);
        }
        public static void EditTerm(Term term) 
        {
            var connection = new SQLiteConnection(DbPath);
            connection.Update(term);
        }

        public static List<Term> QueryTerm()
        { 
            var connection = new SQLiteConnection(DbPath);
            return connection.Query<Term>("SELECT * FROM Term");
        }

        public static void DeleteTerm(Term term)
        {
            var connection = new SQLiteConnection(DbPath);
            connection.Delete(term);
        }

        #endregion

        #region assessments
        public static void AddAssessment(Assessment assessment) 
        {
            var connection = new SQLiteConnection(DbPath);
            connection.Insert(assessment);
        }
        public static void EditAssessment(Assessment assessment) 
        {
            var connection = new SQLiteConnection(DbPath);
            connection.Update(assessment);
        }

        public static List<Assessment> QueryAssessment()
        {
            var connection = new SQLiteConnection(DbPath);
            return connection.Query<Assessment>("SELECT * FROM Assessment");
        }
        #endregion

        #region instructors
        public static void AddInstructor(Instructor Instructor) 
        {
            var connection = new SQLiteConnection(DbPath);
            connection.Insert(Instructor);
        }
        public static void EditInstructor(Instructor Instructor) 
        {
            var connection = new SQLiteConnection(DbPath);
            connection.Update(Instructor);
        }

        public static List<Instructor> QueryInstructor()
        {
            var connection = new SQLiteConnection(DbPath);
            return connection.Query<Instructor>("SELECT * FROM Instructor");
        }
        #endregion

        #region example data methods
        public static void AddStartingTerm()
        {
            AddTerm(new Term("Term 1", DateTime.Now, DateTime.Now.AddMonths(6)));
            AddTerm(new Term("Term 2", DateTime.Now, DateTime.Now.AddMonths(6)));     
        }

        public static void AddStartingNote()
        {
            AddNote(new Note(1, "Example Note 1"));
            AddNote(new Note(1, "Example Note 2"));
        }
        public static void AddExampleData()
        {
            File.Delete(DbPath);
            CreateTable();

            AddStartingTerm();
            AddStartingCourse();
            AddStartingNote();
            AddStartingAsssignment();
            AddStartingInstructor();
        }

        public static void AddStartingCourse()
        {
            AddCourse(new Course(1, 1, "Calculus 1", DateTime.Now, DateTime.Now.AddMonths(5), "In Progress", "Enter Details Here:", 1, 2));
            AddCourse(new Course(1, 1, "English 1", DateTime.Now, DateTime.Now.AddMonths(5), "In Progress", "Enter Details Here:", 3, 4));
            AddCourse(new Course(1, 1, "Science 1", DateTime.Now, DateTime.Now.AddMonths(5), "In Progress", "Enter Details Here:", 1, 1));
            AddCourse(new Course(1, 1, "Chinese 1", DateTime.Now, DateTime.Now.AddMonths(5), "In Progress", "Enter Details Here:", 1, 1));
            AddCourse(new Course(1, 1, "Art History 1", DateTime.Now, DateTime.Now.AddMonths(5), "In Progress", "Enter Details Here:", 1, 1));
            AddCourse(new Course(1, 1, "Computer Science 1", DateTime.Now, DateTime.Now.AddMonths(5), "In Progress", "Enter Details Here:", 1, 1));

            AddCourse(new Course(2, 1, "Calculus 2", DateTime.Now, DateTime.Now.AddMonths(5), "Plan to Take", "Enter Details Here:", 1, 2));
            AddCourse(new Course(2, 1, "English 2", DateTime.Now, DateTime.Now.AddMonths(5), "Plan to Take", "Enter Details Here:", 1, 2));
            AddCourse(new Course(2, 1, "Science 2", DateTime.Now, DateTime.Now.AddMonths(5), "Plan to Take", "Enter Details Here:", 1, 2));
            AddCourse(new Course(2, 1, "Chinese 2", DateTime.Now, DateTime.Now.AddMonths(5), "Plan to Take", "Enter Details Here:", 1, 2));
            AddCourse(new Course(2, 1, "Art History 2", DateTime.Now, DateTime.Now.AddMonths(5), "Plan to Take", "Enter Details Here:", 1, 2));
            AddCourse(new Course(2, 1, "Computer Science 2", DateTime.Now, DateTime.Now.AddMonths(5), "Plan to Take", "Enter Details Here:", 1, 2));
        }

        public static void AddStartingAsssignment()
        {
            AddAssessment(new Assessment(1, "Performance Assessment #1", DateTime.Now, DateTime.Now.AddMonths(2), "Enter details here:", 1));
            AddAssessment(new Assessment(0, "Objective Assessment #1", DateTime.Now, DateTime.Now.AddMonths(2), "Enter details here:", 1));
            AddAssessment(new Assessment(1, "Performance Assessment #1", DateTime.Now, DateTime.Now.AddMonths(2), "Enter details here:", 2));
            AddAssessment(new Assessment(0, "Objective Assessment #1", DateTime.Now, DateTime.Now.AddMonths(2), "Enter details about assessment here:", 2));
            AddAssessment(new Assessment(1, "Performance Assessment #1", DateTime.Now, DateTime.Now.AddMonths(2), "Enter details here:", 3));
            AddAssessment(new Assessment(0, "Objective Assessment #1", DateTime.Now, DateTime.Now.AddMonths(2), "Enter details here:", 3));
            AddAssessment(new Assessment(1, "Performance Assessment #1", DateTime.Now, DateTime.Now.AddMonths(2), "Enter details here:", 4));
            AddAssessment(new Assessment(1, "Performance Assessment #1", DateTime.Now, DateTime.Now.AddMonths(2), "Enter details here:", 5));
            AddAssessment(new Assessment(0, "Objective Assessment #1", DateTime.Now, DateTime.Now.AddMonths(2), "Enter details here:", 4));
            AddAssessment(new Assessment(0, "Objective Assessment #1", DateTime.Now, DateTime.Now.AddMonths(2), "Enter details here:", 5));
            AddAssessment(new Assessment(1, "Performance Assessment #1", DateTime.Now, DateTime.Now.AddMonths(2), "Enter details here:", 6));
            AddAssessment(new Assessment(0, "Objective Assessment #1", DateTime.Now, DateTime.Now.AddMonths(2), "Enter details here:", 6));

        }

        public static void AddStartingInstructor()
        {
            var instructor = new Instructor("Anika Patel", "555-123-4567", "anika.patel@strimeuniversity.edu");
            AddInstructor(instructor);
        }

        #endregion

        public static void Update(object obj)
        {
            var connection = new SQLiteConnection(DbPath);
            connection.Update(obj);
        }
    }
}

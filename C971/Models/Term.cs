﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace C971.Models
{
    [Table("Term")]
    public class Term
    {
        public Term() { }
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public Term(string termName, DateTime start, DateTime end)
        {
            Name = termName;
            Start = start;
            End = end;
        }

    }
}

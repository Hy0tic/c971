﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C971.Models
{
    [Table("Note")]
    public class Note
    {
        public Note() { }
        public Note(int courseId, string content)
        {
            this.CourseId = courseId;
            this.Content = content;
        }
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int CourseId { get; set; }
        public string Content { get; set; }
    }
}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C971.Models
{
    [Table("Assessment")]
    public class Assessment
    {
        public Assessment() { }
        public Assessment(int type, string assessmentName, DateTime start, DateTime end, string assessmentDetails, int courseId)
        {
            Type = type;
            Name = assessmentName;
            Start = start;
            End = end;
            Details = assessmentDetails;
            CourseId = courseId;
            DueDate = end;
        }
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int StartNotif { get; set; }
        public int EndNotif { get; set; }
        public string Details { get; set; }
        public int CourseId { get; set; }
        public DateTime DueDate { get; set; }
    }
}

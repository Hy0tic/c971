﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C971.Models
{
    [Table("Instructor")]
    public class Instructor
    {
        public Instructor() { }
        public Instructor(string instructorName, string instructorPhone, string instructorEmail)
        {
            Name = instructorName;
            PhoneNumber = instructorPhone;
            EmailAddress = instructorEmail;
        }

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
    }
}

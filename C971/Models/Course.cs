﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C971.Models
{
    [Table("Course")]
    public class Course
    {
        public Course() { }
        public Course(int termId, int instructorId, string courseName, DateTime start, DateTime end, string status, string courseDetails, int pa, int oa)
        {
            TermId = termId;
            InstructorId = instructorId;
            Name = courseName;
            Start = start;
            End = end;
            Status = status;
            Details = courseDetails;
            Pa = pa;
            Oa = oa;
        }
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int TermId { get; set; }

        public int InstructorId { get; set; }
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Status { get; set; }
        public string Details { get; set; }
        public int Pa { get; set; }
        public int Oa { get; set; }
        public int StartNotification { get; set; }
        public int EndNotification { get; set; }
    }
}
